package com.examplecalaca.backendCRUD;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProdutoDao extends JpaRepository<Produto, Integer> {
	
	

}
