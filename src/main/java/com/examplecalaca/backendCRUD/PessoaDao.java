package com.examplecalaca.backendCRUD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PessoaDao {
	
	@Autowired
	private DataSource datasource;
	
	
	
	public void salvar(Pessoa pessoa) {
		try (Connection con = datasource.getConnection()){
			PreparedStatement ps = con.prepareStatement("insert into pessoa (nome, endereco) values (?, ?, ?) on conflict (id) do update set nome = ?, endereco = ?");
			int idx = 1;
			ps.setInt(idx++, pessoa.getId());
			ps.setString(idx++, pessoa.getNome());
			ps.setString(idx++, pessoa.getEndereco());
			ps.setString(idx++, pessoa.getNome());
			ps.setString(idx++, pessoa.getEndereco());
			ps.execute();
	}catch (Exception e) {
				// TODO: handle exception
		e.printStackTrace();
			}
		
	}
	
	public void alterar(Pessoa pessoa) {
		try (Connection con = datasource.getConnection()){
			PreparedStatement ps = con.prepareStatement("update pessoa set nome = ?, endereco =? where id =?)");
			ps.setString(1, pessoa.getNome());
			ps.setString(2, pessoa.getEndereco());
			ps.setInt(2, pessoa.getId());
			ps.execute();
	}catch (Exception e) {
				// TODO: handle exception
		e.printStackTrace();
			}
		
	}
	
	public List<Pessoa> listar(){
		List<Pessoa> pessoas = new ArrayList<Pessoa>();
		try (Connection con = datasource.getConnection()){
			PreparedStatement ps = con.prepareStatement("select * from pessoa");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				pessoas.add(mapear(rs));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return pessoas;
		
	}
	
	public List<Pessoa> excluir(int id){
		List<Pessoa> pessoas = new ArrayList<Pessoa>();
		try (Connection con = datasource.getConnection()){
			PreparedStatement ps = con.prepareStatement("delete * from pessoa where id = ?");
			ps.setInt(1, id);
			ps.execute();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return pessoas;
		
	}
	
	private Pessoa mapear(ResultSet rs) throws SQLException {
		Pessoa pessoas = new Pessoa();
		pessoas.setId(rs.getInt("id"));
		pessoas.setNome(rs.getString("nome"));
		pessoas.setEndereco(rs.getString("endereco"));
		
		return pessoas;
	}
	
	

}
